<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

    	//Clear All Data
        DB::table('days')->truncate();
        DB::table('modules')->truncate();
        DB::table('users')->truncate();

        // Insert Data

        // Days
        DB::table('days')->insert([
        		'date' => '2015-08-04',
        	]);

        DB::table('days')->insert([
        		'date' => '2015-08-05',
        	]);

        DB::table('days')->insert([
        		'date' => '2015-08-06',
        	]);

        DB::table('days')->insert([
        		'date' => '2015-08-07',
        	]);


        // Modules
        DB::table('modules')->insert([
        		'name' => 'Casual Dining',
        		'description' => 'This dining is like a bistro/café.',
        		'configuration' => 'Tables of 2 and 4',
        		'time' => '10:50 - 12:30',
        		'totalRSC' => 6,
        		'totalSPD' => 36,
        	]);

        DB::table('modules')->insert([
        		'name' => 'Casual Dining',
        		'description' => 'This dining is like a bistro/café.',
        		'configuration' => 'Tables of 2 and 4',
        		'time' => '13:30 - 14:40',
        		'totalRSC' => 6,
        		'totalSPD' => 36,
        	]);

        DB::table('modules')->insert([
        		'name' => 'Bar Service',
        		'description' => 'Casual service for sandwiches, cakes, cheese plates, salads, alcoholic and non-alcoholic beverages. Guests can choose from a limited menu. Competitors will prepare international cocktails and serve with light snacks.',
        		'configuration' => 'Tables of 6',
        		'time' => '13:15 - 14:45',
        		'totalRSC' => 6,
        		'totalSPD' => 36,
        	]);

        DB::table('modules')->insert([
        		'name' => 'Fine Dining',
        		'description' => 'This is formal dining with a four course set menu with alcoholic beverages. The service includes the waiter preparing all dishes at the table by flambé, carving or assembling. Appropriate for VIPs.',
        		'configuration' => 'Tables of 4',
        		'time' => '13:00 - 15:15',
        		'totalRSC' => 6,
        		'totalSPD' => 24,
        	]);

        DB::table('modules')->insert([
        		'name' => 'Banquet Dining',
        		'description' => 'This is a three course set menu with coffee and alcoholic beverages in a banquet format.',
        		'configuration' => 'Tables of 6',
        		'time' => '12:45 - 15:00',
        		'totalRSC' => 6,
        		'totalSPD' => 36,
        	]);

        DB::table('users')->insert([
                'name'      => 'Administrator',
                'email'     => 'admin@admin.com',
                'password'  => bcrypt('nopass'),
            ]);


    }
}
