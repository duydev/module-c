Booking confirmation

{{ $b->name }}, Thank you for your booking request {{ $b->id }}.

We are happy to send you the latest information about guest confirmation:

@foreach ($q as $_q)
{{ $_q->title }}
{{ $_q->info }}

@endforeach

Please note that your guests need to arrive at Restaurant Service at least 10 minutes prior to thescheduled.
