@extends('layouts.app')

@section('title','Information')

@section('content')
	<div id="dining_experience_descriptions">
        <div class="">
            <div class="row">
                <div class="col-xs-6">
                    <h1>Guests in Restaurant Service</h1>
                    <p class="clearfix">Become part of the competition: be a guest in Restaurant Service competition by requesting a seat and enjoy one of the different dining experiences!</p>
                </div>
                <div class="col-xs-offset-2 col-xs-4 col-sm-offset-4 col-sm-2">
                    <h1><img src="img/6215177259.jpg" alt="cook in restaurant service" class="img-thumbnail img-responsive"></h1>
                </div>
            </div>
            <h3>Dining experience desriptions</h3>
            <table class="table table-bordered table-striped">
                <colgroup>
                    <col style="width: 25%">
                    <col style="width: 25%">
                    <col style="width: 25%">
                    <col style="width: 25%">
                </colgroup>
                <thead>
                    <tr>
                        <th>Casual Dining</th>
                        <th>Bar Service</th>
                        <th>Fine Dining</th>
                        <th>Banquet Dining</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>This dining is like a bistro/café.</td>
                        <td>Casual service for sandwiches, cakes, cheese plates, salads, alcoholic and non-alcoholic beverages. Guests can choose from a limited menu. Competitors will prepare international cocktails and serve with light snacks.</td>
                        <td>This is formal dining with a four course set menu with alcoholic beverages. The service includes the waiter preparing all dishes at the table by flambé, carving or assembling. Appropriate for VIPs.</td>
                        <td>This is a three course set menu with coffee and alcoholic beverages in a banquet format.</td>
                    </tr>
                    <tr>
                        <td>Tables of 2 and 4</td>
                        <td>Tables of 6</td>
                        <td>Tables of 4</td>
                        <td>Tables of 6</td>
                    </tr>
                    <tr>
                        <td>
                            <p>Seating 1: 10:50 - 12:30</p>
                            <p>Seating 2: 13:30 - 14:40</p>
                        </td>
                        <td>Seating: 13:15 - 14:45</td>
                        <td>Seating: 13:00 - 15:15</td>
                        <td>Seating: 12:45 - 15:00</td>
                    </tr>
                </tbody>
            </table>
            <form action="/booking">
                <button class="btn btn-primary" type="submit" name="start-booking">Start booking</button>
            </form>
        </div>
    </div>
@endsection