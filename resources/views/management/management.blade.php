@extends('layouts.app')

@section('title','Management')

@section('content')
<div id="reservation_management">
    <h1>Reservation management</h1>
    <form action="{{ url('/management') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <fieldset>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <colgroup>
                        <col style="width: 5%">
                            <col style="width: 30%">
                                <col style="width: 10%">
                                    <col style="width: 30%">
                                        <col style="width: 10%">
                                            <col style="width: 5%">
                                                <col style="width: 5%">
                                                    <col style="width: 5%">
                                                        <col style="width: 5%">
                    </colgroup>
                    <thead>
                        <tr>
                            <th rowspan="2">Day</th>
                            <th rowspan="2">Seating</th>
                            <th rowspan="2">Booking No.</th>
                            <th rowspan="2">Guests</th>
                            <th rowspan="2">Status</th>
                            <th colspan="4">Action</th>
                        </tr>
                        <tr>
                            <th>Confirm</th>
                            <th>Decline</th>
                            <th>Waitlist</th>
                            <th>Reschedule</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($details as $d) 
                            @if($d->status == "requested") 
                                @include('management.row_r') 
                            @else 
                                @include('management.row_o') 
                            @endif 
                            <!-- {{ $a[$d->booking_id]++ }} --> 
                        @endforeach
                    </tbody>
                </table>
            </div>
        </fieldset>
        <button class="btn btn-default" type="submit" name="create-guest-list">Create guest list</button>
        <button class="btn btn-default" type="submit" name="send-emails">Send emails</button>
        <button class="btn btn-primary" type="submit" name="save-confirmations">Save changes</button>
    </form>
</div>
@endsection

