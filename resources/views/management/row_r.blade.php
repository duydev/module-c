<tr>
    {{-- If is reschedule --}}
    @if( in_array($d->id, $res_id) )
        <td>
            <select name="{{ $d->id }}-d">
                @foreach( $days as $_d )     
                    <option value="{{ $_d->id }}" {{ $d->day->id == $_d->id ? 'selected="selected"' : '' }} >C{{ $_d->id }}</option>
                @endforeach
            </select>
        </td>
        <td>
            <select name="{{ $d->id }}-m">
                @foreach($modules as $_m)
                    <option value="{{ $_m->id }}" {{ $d->module->name." ".$d->module->time == $_m->name." ".$_m->time ? 'selected="selected"' : '' }} >{{ $_m->name." ".$_m->time }}</option>
                @endforeach
            </select>
        </td>
    @else
        <td>C{{ $d->day->id }}</td>
        <td>{{ $d->module->name }} {{ $d->module->time }}</td>
    @endif
    <td>
        <span title="{{ $d->booking->name }}, {{ $d->booking->organization }}, {{ $d->booking->phone }}, {{ $d->booking->email }}, {{ $d->booking->country }}">
            {{ $d->booking_id }}
        </span>
    </td>
    
    <td>{{ $a[$d->booking_id] }}. {{ $d->name }} {{ $d->country }}</td>
    <td>requested</td>
    <td>
        <p class="text-center">
            <input type="radio" name="{{ $d->booking_id }}-{{ $d->id }}" value="confirm" {{ in_array($d->id, $res_id)? "checked" : "" }}></p>
    </td>
    <td>
        <p class="text-center">
            <input type="radio" name="{{ $d->booking_id }}-{{ $d->id }}" value="decline" {{ in_array($d->id, $res_id)? "disabled" : "" }}></p>
    </td>
    <td>
        <p class="text-center">
            <input type="radio" name="{{ $d->booking_id }}-{{ $d->id }}" value="waitlist" {{ in_array($d->id, $res_id)? "disabled" : "" }}></p>
    </td>
    <td>
        <p class="text-center">
            <input type="radio" name="{{ $d->booking_id }}-{{ $d->id }}" value="reschedule" {{ in_array($d->id, $res_id)? "disabled" : "" }}></p>
    </td>
</tr>