<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Guests for Restaurant Service | @yield('title')</title>
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Bootstrap core CSS -->
        <link href="/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="/css/restaurantapp.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="/js/html5shiv.3.7.0.js"></script>
          <script src="/js/respond.min.1.3.0.js"></script>
        <![endif]-->
    </head>
    <body>
        <a class="sr-only sr-only-focusable" href="#content" tabindex="1"><div class="container"><span class="skiplink-text">Skip to main content</span></div></a>

        <div class="navbar navbar-worldskills navbar-static-top">
            <div class="cube-container">
                <div class="cube-right-bottom-blue">&nbsp;</div>
            </div>
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">Reservations</a>
                </div>
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/information">Information</a></li>
                        <li><a href="/booking">Booking</a></li>
                        <li><a href="/management">Management</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <!-- use the following link for login and logoff -->
                        @if(Auth::guest())
                        <li><a href="/login">Login</a></li>
                        @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                               Hi, {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/logout"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        @endif
                    </ul>
                </div>
            </div>
        </div>

        <div class="container">

            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">@yield('title')</li>
            </ol>

                    <div id="content">
                    @yield('content')
                    </div>

            <footer>
                <hr class="hr-extended" />
                <p>© 2015 WorldSkills</p>
            </footer>

        </div>

    <!-- Bootstrap core JS -->
        <script src="/js/jquery.min.1.11.1.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/restaurantapp.js"></script>
        @yield('js');
    </body>
</html>
