@extends('layouts.app')

@section('title','Booking')

@section('content')
<div id="booking_request">
    <h1 class="page-header">Booking request</h1>
    <!-- individual (depending on selected button on form before) -->
    <form action="{{ url('/booking/individual') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <fieldset>
            <legend>Individual</legend>
            <p>Booking an individual guest</p>
            <!-- message -->
            <div class="error-message">@include('common.errors')</div>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                        <th>Dining experience</th>
                        @foreach($days as $day)
                        <th>C{{ $day->id }}: {{ date('d.m.Y', strtotime($day->date)) }}</th>
                        @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($modules as $module)
                        <tr>
                            <td>{{ $module->name }}<br/>{{ $module->time }}</td>
                            @foreach($days as $day)
                            <td>available: {{ $availables[$module->id][$day->id] }} <input type="checkbox" name="c{{ $day->id }}-d{{ $module->id }}-n1"></td>
                            @endforeach
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <p>Please note that most seating take place at the same time and you are not allowed to change once seated.<br />For a seating that is full, you will be waitlisted.</p>
        </fieldset>
        <button class="btn btn-primary" type="submit" name="book-individual">Submit booking request</button>
    </form>
</div>
@endsection