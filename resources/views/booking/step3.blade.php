@extends('layouts.app')

@section('title','Booking')

@section('content')
<div id="submission_confirmation">
    <h1 class="page-header">Submission confirmation</h1>
    <p>
    {{ $guest['name'] }},<br/><br/>
    Thank you for your booking request {{ $bookno }}.<br/><br/>

    You have requested booking for the following guests:
    </p>
    <ul>
        @foreach($infos as $info)
            <li>{{ $info->info }} <br/>for {{ $info->person }}</li>
        @endforeach
    </ul>
    <p>
    Please note that these booking requests will need to be reviewed and confirmed by WSI. <br/>
    You will receive an email with the confirmation as soon as possible.
    </p>
</div>
@endsection