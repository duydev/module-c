@extends('layouts.app')

@section('title','Booking')

@section('content')
<div id="booking_request">
    <h1 class="page-header">Booking request</h1>
    <!-- group (depending on selected button on form before) -->
    <form action="{{ url('/booking/group') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <fieldset>
            <legend>Group</legend>
            <p>Booking a group</p>
            <!-- message -->
            <div class="error-message">@include('common.errors')</div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                @foreach($days as $day)
                <li role="presentation"><a href="#c{{$day->id}}" aria-controls="c{{$day->id}}" role="tab" data-toggle="tab">C{{$day->id}}: {{ date('d.m.Y', strtotime($day->date)) }}</a></li>
                @endforeach
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                @foreach($days as $day)
                <div role="tabpanel" class="tab-pane" id="c{{ $day->id }}">
                    <input type="hidden" name="day" value="c{{ $day->id }}">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                            <th>Dining experience</th>
                            <th>Number of seats available<br/>Number of guests to be seated</th>
                            <th>Guest names (if known)</th>
                            <th>Guest country*</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($modules as $module)
                            <tr>
                                <td>{{ $module->name }}<br/>{{ $module->time }}</td>
                                <td>
                                    available: {{ $availables[$module->id][$day->id] }} <br/><button type="button" class="btn btn-default addguest" id="c{{$day->id}}-d{{$module->id}}" onclick="">+ Add guest</button>
                                </td>
                                <td id="c{{$day->id}}-d{{$module->id}}-n">
                                    <p><input type="text" id="c{{$day->id}}-d{{$module->id}}-n1" name="c{{$day->id}}-d{{$module->id}}-n1" class="form-control"></p>
                                </td>
                                <td id="c{{$day->id}}-d{{$module->id}}-o">
                                    <p>
                                    <select id="c{{$day->id}}-d{{$module->id}}-o1" name="c{{$day->id}}-d{{$module->id}}-o1" class="form-control">
                                        <option value="">choose a country</option>
                                        <option value="AU">AU - Australia</option>
                                        <option value="BR">BR - Brasil</option>
                                        <option value="CA">CA - Canada</option>
                                        <option value="CH">CH - Switzerland</option>
                                        <option value="CN">CN - China</option>
                                        <option value="DE">DE - Germany</option>
                                        <option value="FR">FR - France</option>
                                        <option value="IN">IN - India</option>
                                    </select>
                                    </p>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
                @endforeach
            </div>
        </fieldset>
        <button class="btn btn-primary" type="submit" name="book-group">Submit booking request</button>
    </form>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $('ul.nav-tabs li').first().addClass('active');      
        $('#c1').addClass('active');
    });
</script>
@endsection