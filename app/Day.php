<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\BookingDetail;

class Day extends Model
{
    protected $fillable = ['date'];
    public $timestamps = false;

    public function booking_details()
    {
    	return $this->hasMany(BookingDetail::class);
    }
}
