<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\BookingDetail;

class Booking extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    public function booking_detail()
    {
    	return $this->hasMany(BookingDetail::class);
    }
}
