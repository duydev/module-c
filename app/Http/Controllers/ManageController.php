<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Http\Requests;
use App\Day;
use App\Module;
use App\Booking;
use App\BookingDetail;
use DB;

class ManageController extends Controller
{

    public function index()
    {
        // Reschedule ID
        $res_id = session('res_id') ? session('res_id') : array();
        
        $days = Day::all();
        $modules = Module::all();
        $details = BookingDetail::all()->sortBy(['day_id','module_id','booking_id']);

        $a = array();
        foreach ($details->unique()->values()->all() as $d) {
            $a[$d->booking_id] = 1;
        }

    	return view('management.management', [
                'days'      => $days,
    			'modules'   => $modules,
                'details'   => $details,
                'a'         => $a,
                'res_id'    => $res_id,
    		]);
    }

    public function store(Request $req)
    {
        $_req = $req->except(['_token']);

        $action = "";

        if(isset($_req['create-guest-list']))
            $action = 'create-guest-list';
        else if(isset($_req['send-emails']))
            $action = 'send-emails';
        else if(isset($_req['save-confirmations']))
            $action = 'save-confirmations';

        switch ($action) {
            case 'save-confirmations':
                    // Reschedule ID
                    $res_id = array();
                    foreach($_req as $key => $value)
                    {
                        preg_match('/(.*?)-(.*?)$/', $key, $matches);

                        switch ($value) {
                            case 'reschedule':
                                    $res_id[] = $matches[2]; 
                                break;
                            case 'confirm':
                                    $b_d = BookingDetail::find($matches[2]);
                                    if($req->has($matches[2].'-d') && $req->has($matches[2].'-m'))
                                    {
                                        $b_d->day_id    = $req->input($matches[2].'-d');
                                        $b_d->module_id = $req->input($matches[2].'-m');
                                    }
                                    $b_d->status = 'confirmed';
                                    $b_d->save();
                                break;
                            case 'decline':
                                    BookingDetail::where('id', $matches[2])->update(['status' => 'declined']);
                                break;
                            case 'waitlist':
                                    BookingDetail::where('id', $matches[2])->update(['status' => 'waitlisted']);
                                break;
                        }
                    }
                    return redirect()->action('ManageController@index')->with('res_id', $res_id);
                break;
            case 'send-emails':
                   
                    $bookings = Booking::all();    
                    foreach ($bookings as $b) {
                   
                        $q = DB::select("SELECT booking_id, CONCAT('C', days.id, ' - ', days.date, ', ', modules.name, ' ', modules.time) AS title, GROUP_CONCAT(booking_details.name, ' ', country, ' - ', status SEPARATOR '\r\n') AS info FROM booking_details, days, modules WHERE booking_details.day_id = days.id AND booking_details.module_id = modules.id AND booking_id = ? GROUP BY booking_id, title",[$b->id]);     
                   
                        $content = view('email', [
                            'b' => $b,
                            'q' => $q,
                            ])->render();

                        Storage::disk('local')->put('emails/'.$b->id.'.txt', $content);
                    }                    

                    return redirect()->action('ManageController@index');
                break;
            // Export CSV
            case 'create-guest-list':

                    $list = array (
                        array('aaa', 'bbb', 'ccc', 'dddd'),
                        array('123', '456', '789'),
                        array('"aaa"', '"bbb"')
                    );

                    $fp = fopen('file.csv', 'w');

                    foreach ($list as $fields) {
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);

                    return redirect()->action('ManageController@index');
                break;
        }
        
    }

}
