<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\Step1Request;
use App\Http\Controllers\Controller;
use App\Day;
use App\Module;
use App\Booking;
use App\BookingDetail;
use DB;

class BookingController extends Controller
{

	function index()
	{
		return view('booking.step1');
	}

	function step1(Step1Request $req)
	{
		// Info Person Booking
		$guest = array(
				'name' 			=> $req->name,
				'organization' 	=> $req->organization,
				'email'			=> $req->email,
				'phone'			=> $req->phone,
				'country'		=> $req->country,
			);

		// Store Person Booking Data 
		$req->session()->put('guest', $guest);

		$days = Day::all();
		$modules = Module::all();

		$availables = array();
		
		// Calc available seating number
		foreach ($modules as $m) {
			$a = array();
			foreach ($days as $d) {
				$b = BookingDetail::where([
						['module_id', $m->id],
						['day_id', $d->id],
						['status', 'confirmed'],
					])->count();
				$a[$d->id] = $m->totalSPD - $b; 
			}
			
			$availables[$m->id] = $a;
		}

		if($req->input('agree-group') !== NULL)
		{
			return view('booking.step2group', [
				'days' => $days,
				'modules' => $modules,
				'availables' => $availables,
			]);
		}
		else 
		{
			return view('booking.step2individual', [
				'days' => $days,
				'modules' => $modules,
				'availables' => $availables,
			]);
		}
	}

	function step2g(Request $req)
	{
		// Info Person Booking
		$guest = $req->session()->get('guest');
		//$req->session()->forget('guest');
		
		// Guest Choose
		$chooses = $req->except(['_token','day', 'book-group']);

		// Booking Success
		$id = Booking::insertGetId([
				'date' 			=> date('Y-m-d'),
				'name' 			=> $guest["name"],
				'organization' 	=> $guest["organization"],
				'phone'			=> $guest["phone"],
				'email'			=> $guest["email"],
				'country'		=> $guest["country"],
			]);

		foreach ($chooses as $key => $value) {
			// If empty Country or Empty name skip input
			if(empty($value) || !preg_match('/c(.*?)-d(.*?)-o(.*?)$/', $key, $matches))
				continue;
			list($t, $d, $m, $c) = $matches;

			// Guest Name
			$name = $req->input('c'.$d.'-d'.$m.'-n'.$c);
			// Guest Country
			$country = $value;

			BookingDetail::insert([
					'day_id' 		=> $d,
					'module_id' 	=> $m,
					'booking_id' 	=> $id,
					'name' 			=> !empty($name) ? $name : $guest['organization'],
					'country'		=> $country,
					'status'		=> 'requested',
				]);
		}

		$infos = DB::select("SELECT day_id, module_id, CONCAT('C',days.id,' - ',days.date, ', ',modules.name,' ',modules.time) AS info, GROUP_CONCAT(booking_details.name, ' ', booking_details.country SEPARATOR ', ') AS person FROM booking_details, days, modules WHERE days.id = booking_details.day_id AND modules.id = booking_details.module_id AND booking_id = ? GROUP BY module_id ORDER BY day_id, module_id", [$id]);

		return view('booking.step3', [
				'bookno' => $id,
				'infos' => $infos,
				'guest' => $guest,
			]);
	}

	function step2i(Request $req)
	{
		
		// Info Person Booking
		$guest = $req->session()->get('guest');
		$req->session()->forget('guest');

		// Guest Choose
		$chooses = $req->except(['_token','book-individual']);

		// Booking Success
		$id = Booking::insertGetId([
				'date' 			=> date('Y-m-d'),
				'name' 			=> $guest["name"],
				'organization' 	=> $guest["organization"],
				'phone'			=> $guest["phone"],
				'email'			=> $guest["email"],
				'country'		=> $guest["country"],
			]);
		
		foreach ($chooses as $key => $value) {
			preg_match('/c(.*?)-d(.*?)-n1/', $key, $matches);
			list($t, $d, $m) = $matches;
			BookingDetail::insert([
					'day_id' 		=> $d,
					'module_id' 	=> $m,
					'booking_id' 	=> $id,
					'name' 			=> $guest["name"],
					'country'		=> $guest["country"],
					'status'		=> 'requested',
				]);
		}
		
		$infos = DB::select("SELECT day_id, module_id, CONCAT('C',days.id,' - ',days.date, ', ',modules.name,' ',modules.time) AS info, GROUP_CONCAT(booking_details.name, ' ', booking_details.country SEPARATOR ', ') AS person FROM booking_details, days, modules WHERE days.id = booking_details.day_id AND modules.id = booking_details.module_id AND booking_id = ? GROUP BY module_id ORDER BY day_id, module_id", [$id]);

		return view('booking.step3', [
				'bookno' => $id,
				'infos' => $infos,
				'guest' => $guest,
			]);
	}

	


}
	