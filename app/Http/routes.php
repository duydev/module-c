<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    
    Route::get('/', function () {
	    return view('information');
	});

	Route::get('/information', function () {
	    return view('information');
	});

	Route::get('/booking', 'BookingController@index');
	Route::post('/booking', 'BookingController@step1');
	Route::post('/booking/individual', 'BookingController@step2i');
	Route::post('/booking/group', 'BookingController@step2g');
	Route::get('/booking/{s}', function(){
		return redirect('/booking');
	});
	
	// Management
	Route::group(['middleware' => ['auth']], function () {
		Route::get('/management', 'ManageController@index');
		Route::post('/management', 'ManageController@store');
	});

    Route::auth();

});
