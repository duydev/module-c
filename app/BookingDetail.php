<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Day;
use App\Module;
use App\Booking;

class BookingDetail extends Model
{
	protected $guarded = ['id'];
	public $timestamps = false;

	public function day()
	{
		return $this->belongsTo(Day::class);
	}

	public function module()
	{
		return $this->belongsTo(Module::class);
	}

	public function booking()
	{
		return $this->belongsTo(Booking::class);
	}
}
